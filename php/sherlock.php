<?php
// List of the dictionaries titles
$dictionaries = array('Le chien des Baskerville - Sir Arthur Conan Doyle','L\'Empire romain apr&egrave;s la paix de l\'&eacute;glise - Cte de Montalbert','Les Mis&eacute;rables / Livre Premier / Tome 1 - Victor Hugo');
?>

<h1>Encode</h1>
<form action="process.php" method="post">
    <input type="hidden" name="type" value="1" />
    <select name="dictionary">
        <?php
        foreach ($dictionaries as $key=>$value)
        {
        ?>
        <option value="<?php echo $key;?>"><?php echo $value;?></option>
        <?php
        }
        ?>
    </select><br />
    <input type="checkbox" name="heuristic" value="10" /> Coder m&ecirc;me les mots n'existant pas dans le dictionnaire avec un d&eacute;calage de : <input type="text" value="0" name="shift"/><br />
    Ajouter du bruit : <select name="day"><option value="">--</option>
        <?php
        for ($i=1;$i<=31;$i++)
        {
            echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
    </select>/
    <select name="month"><option value="">--</option>
        <?php
        for ($i=1;$i<=12;$i++)
        {
            echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
    </select>/
    <select name="year"><option value="">--</option>
        <?php
        for ($i=1920;$i<=2000;$i++)
        {
            echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
    </select>
    <br />
    <textarea name="text" cols="160" rows="12"></textarea>
    <input type="submit" value="OK" />
</form>

<h1>Decode</h1>
<form action="process.php" method="post">
    <input type="hidden" name="type" value="2" />
    <select name="dictionary">
        <?php
        foreach ($dictionaries as $key=>$value)
        {
        ?>
        <option value="<?php echo $key;?>"><?php echo $value;?></option>
        <?php
        }
        ?>
    </select><br />
    <input type="checkbox" name="heuristic" value="10" /> D&eacute;coder m&ecirc;me les mots n'existant pas dans le dictionnaire avec un d&eacute;calage de : <input type="text" value="0" name="shift"/><br />
    Retirer le bruit : <select name="day"><option value="">--</option>
        <?php
        for ($i=1;$i<=31;$i++)
        {
            echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
    </select>/
    <select name="month"><option value="">--</option>
        <?php
        for ($i=1;$i<=12;$i++)
        {
            echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
    </select>/
    <select name="year"><option value="">--</option>
        <?php
        for ($i=1920;$i<=2000;$i++)
        {
            echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
    </select>
    <br />
    <textarea name="text" cols="160" rows="12"></textarea>
    <input type="submit" value="OK" />
</form>
