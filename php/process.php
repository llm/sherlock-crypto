<?php
// check
$type=$_POST['type'];
$dic=$_POST['dictionary'];
$text=$_POST['text'];
$heuristic=$_POST['heuristic'];
$shift=$_POST['shift'];
$noiseD=$_POST['day'];
$noiseM=$_POST['month'];
$noiseY=$_POST['year'];
if (!is_null($type) && !is_null($dic) && is_numeric($type) && is_numeric($dic) && (is_null($heuristic) || $heuristic==='10') && is_numeric($shift))
{
    if (!(is_numeric($noiseD) && is_numeric($noiseM) && is_numeric($noiseY)))
    {
        $noiseD=0;
        $noiseM=0;
        $noiseY=0;
    }
    $dictionaries = array('Le chien des Baskerville.txt','empire.txt','Les miserables.txt');
    $expreg='/[^\w|\s|\r]/';
    $mydic = file_get_contents('dictionary/'.$dictionaries[$dic]);
    $mydic = preg_replace($expreg,'',$mydic);
    $dicarray = explode(' ',$mydic);
    $lengthdic = sizeof($dicarray);
    $result='';
    $heuristic = $heuristic==10;
    
    $noise = $noiseD > 0 && $noiseM > 0 && $noiseY > 0;
    
    if ($type==1)
    {
        $encode = preg_replace($expreg,'',$text);
        $crypt = explode(' ',$encode);
        
        
        $incomplete=false;
        $cptD=0;
        $cptM=0;
        foreach ($crypt as $word)
        {
            $keys = array_keys($dicarray,$word);
            $size = sizeof($keys);
            $position=0;
            
            if($noise && $cptD % $noiseD == 1)
            {
                $nbNoise = abs($noiseM + $cptM - $noiseD);
                for ($i=0;$i<$nbNoise;$i++)
                {
                    $result.=rand(1,$lengthdic).'/';
                    $cptM++;
                }
                
            }
            
            if ($size > 1)
            {
                $position = rand(0,$size-1);
            }
            
            if ($size == 0)
            {
                if ($heuristic)
                {
                    $result.='(';
                    for ($i=0;$i<strlen($word);$i++)
                    {
                        $ord=ord($word[$i])+$shift;
                        $result.=$ord;
                        if ($i<strlen($word)-1)
                        {
                            $result.='.';
                        }
                    }
                    $result.=')';
                }
                else
                {
                    $result.='?';
                }
                $incomplete=true;
            }
            else
            {
                $result.=$keys[$position];
            }
            $result.='/';
            $cptD++;
        }
    }
    elseif ($type==2)
    {
        $decode_array = explode('/',$text);
        $cptD=0;
        $cptM=0;
        $nbNoise=0;
        $go=true;
        foreach ($decode_array as $key)
        {
            $go=true;
            if($noise && $cptD % $noiseD == 0)
            {
                
                $nbNoise = abs($noiseM + $cptM - $noiseD);
                $go=true;
            }
            elseif ($nbNoise > 0)
            {
                $go=false;
                $nbNoise--;
                $cptM++;
            }

            if ($go)
            {
                if (preg_match('/\(.*\)/',$key))
                {
                    $key=preg_replace('/[\(\)]/','',$key);
                    $chars = explode('.',$key);
                    foreach ($chars as $char)
                    {
                        $result.=chr($char-$shift);
                    }
                    $result.=' ';
                }
                else
                {
                    $result.=$dicarray[$key].' ';
                }
                $cptD++;
            }
            
        }
    }
}
?>
<h1>Result</h1>
<pre><?php echo $result; ?></pre>
<a href="sherlock.php">Back</a>
