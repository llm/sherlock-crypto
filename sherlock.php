<html>
    <head>
        <title>Sherlock Crypto : A Javascript encryption tool</title>
        <link rel="stylesheet" type="text/css" href="/css/main.css" />
    </head>
    <body>
        <script src="js/sherlock-crypto.js" charset="utf-8" type="text/javascript"></script>
        <script src="js/jquery.min.js"></script>
<?php
$dictionaries = array('Le chien des Baskerville - Sir Arthur Conan Doyle','L\'Empire romain apr&egrave;s la paix de l\'&eacute;glise - Cte de Montalbert','Les Mis&eacute;rables / Livre Premier / Tome 1 - Victor Hugo');
?>
<div class="center">
	<div class="block header">
            Sherlock Crypto : A Javascript encryption tool
        </div>
        <div class="block content">
        <select name="dictionary" id="dictionary">
            <?php
            foreach ($dictionaries as $key=>$value)
            {
            ?>
            <option value="<?php echo $key;?>"><?php echo $value;?></option>
            <?php
            }
            ?>
        </select><br />
        <input type="checkbox" name="heuristic" id="heuristic" value="10" /> Encrypt even the words not found in the dictionary 
        <br />
        <textarea name="enc_text" id="enc_text" cols="160" rows="12"></textarea>
        <br />
        <input type="button" value="Encrypt" onclick="encrypt();"/> <input type="button" value="Decrypt" onclick="decrypt();"/>
        <pre id="result">Result</div>
        </div>
    </div>
    </body>
</html>